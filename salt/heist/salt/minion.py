import asyncio
import copy
import os
import pathlib
import secrets
import sys
from distutils.version import LooseVersion
from typing import Any
from typing import Dict


async def run(
    hub,
    remotes: Dict[str, Dict[str, str]],
    artifact_version=None,
    **kwargs,
):
    coros = []

    for id_, remote in remotes.items():
        coro = hub.heist.salt.minion.single(
            remote,
            artifact_version=artifact_version,
        )
        coros.append(coro)
    await asyncio.gather(*coros, loop=hub.pop.Loop, return_exceptions=False)


async def single(
    hub,
    remote: Dict[str, Any],
    artifact_version=None,
):
    """
    Execute a single async connection
    """
    # create tunnel
    target_name = secrets.token_hex()
    hub.heist.ROSTERS[target_name] = copy.copy(remote)
    tunnel_plugin = remote.get("tunnel", "asyncssh")
    minion_id = hub.heist.ROSTERS[target_name]["id"]
    await hub.heist.salt.minion.manage_tunnel(target_name, tunnel_plugin, remote=remote)

    target_os, target_os_arch = await hub.tool.system.os_arch(
        target_name, tunnel_plugin
    )

    repo_data = await hub.artifact.salt.repo_data()
    if isinstance(repo_data, dict) and not artifact_version:
        artifact_version = max(repo_data.keys(), key=lambda x: LooseVersion(x))

    if artifact_version:
        if not await hub.artifact.salt.get(
            target_name,
            tunnel_plugin,
            target_os,
            repo_data,
            version=artifact_version,
        ):
            return False

    # Get salt minion user
    user = hub.heist.ROSTERS[target_name].get("username")
    if not user:
        user = hub.heist.init.default(target_os, "user")

    run_dir = hub.tool.path.path_convert(
        target_os, "/var", (["tmp", f"heist_{user}", f"{secrets.token_hex()[:4]}"])
    )
    hub.heist.CONS[target_name] = {"run_dir": run_dir}

    # Deploy
    binary = hub.artifact.salt.latest("salt", version=artifact_version)
    binary_path = await hub.artifact.salt.deploy(
        target_name, tunnel_plugin, run_dir, binary
    )
    if not binary_path:
        hub.log.error(f"Could not deploy the artifact to the target {remote['id']}")
        return False

    grains = await hub.salt.call.init.get_grains(
        target_name, tunnel_plugin, binary_path, run_dir
    )
    service_plugin = hub.service.init.get_service_plugin(remote, grains)

    hub.heist.CONS[target_name].update(
        {
            "tunnel_plugin": tunnel_plugin,
            "manager": "salt.minion",
            "bin": binary,
            "binary_path": binary_path,
            "service_plugin": service_plugin,
            "key_plugin": hub.OPT.heist.key_plugin,
        }
    )
    minion_opts = hub.tool.config.get_minion_opts(
        run_dir=run_dir, target_name=target_name
    )
    await hub.heist.salt.minion.manage_tunnel(
        target_name,
        tunnel_plugin,
        create=False,
        tunnel=True,
        minion_opts=minion_opts,
    )

    # Start minion
    hub.log.debug(f"Target '{remote.id}' is using service plugin: {service_plugin}")
    await hub.service.salt.minion.start(
        target_name,
        tunnel_plugin,
        service_plugin,
        run_dir=run_dir,
        binary_path=binary_path,
    )

    if hub.OPT.heist.accept_keys:
        await hub.salt.key.init.accept_key_master(
            target_name, tunnel_plugin, binary_path, run_dir
        )

    hub.log.debug(
        f"Starting infinite loop on {remote.id}.  Checkin time: {hub.OPT.heist.checkin_time}"
    )

    while True:
        if not hub.tunnel[tunnel_plugin].connected(target_name):
            # we lost connection, lets check again to see if we can connect
            hub.log.error(f"Lost connection to {minion_id}, trying to reconnect")
            if await hub.heist.salt.minion.manage_tunnel(
                target_name,
                tunnel_plugin,
                remote=remote,
                create=True,
                tunnel=True,
                reconnect=True,
                minion_opts=minion_opts,
            ):
                hub.log.info(f"Reconnected to {minion_id} successfully.")

        await asyncio.sleep(hub.OPT.heist.checkin_time)
        if hub.OPT.heist.dynamic_upgrade:
            latest = hub.artifact.salt.latest("salt")
            if latest != binary:
                binary = latest
                await hub.artifact.salt.update(
                    target_name, tunnel_plugin, latest, binary_path, run_dir
                )


async def clean(hub, target_name, tunnel_plugin, service_plugin, vals):
    """
    Clean up the connections
    """
    # clean up service files
    await hub.service.init.clean(
        target_name, tunnel_plugin, "salt-minion", service_plugin
    )
    # clean up run directory and artifact
    await hub.artifact.init.clean(target_name, tunnel_plugin)

    minion_id = hub.heist.CONS[target_name].get("minion_id")
    if minion_id:
        if not hub.salt.key.init.delete_minion(minion_id):
            hub.log.error(f"Could not delete the key for minion: {minion_id}")


async def manage_tunnel(
    hub,
    target_name,
    tunnel_plugin,
    remote=None,
    create=True,
    tunnel=False,
    reconnect=False,
    minion_opts=None,
):
    # Create tunnel back to master
    if create:
        created = await hub.tunnel[tunnel_plugin].create(
            target_name, remote, reconnect=reconnect
        )
        if not created:
            hub.log.error(f'Connection to host {remote["host"]} failed')
            return False

        hub.log.info(f'Connection to host {remote["host"]} success')

    if tunnel:
        if not hub.heist.ROSTERS[target_name].get("bootstrap"):
            import salt.config
            import salt.syspaths

            master_opts = salt.config.client_config(
                pathlib.Path(salt.syspaths.CONFIG_DIR) / "master"
            )
            await hub.tunnel[tunnel_plugin].tunnel(
                target_name,
                minion_opts["publish_port"],
                master_opts.get("publish_port", 4505),
            )
            await hub.tunnel[tunnel_plugin].tunnel(
                target_name,
                minion_opts["master_port"],
                master_opts.get("master_port", 4506),
            )
    return True
