=============
Salt Artifact
=============

Heist-salt uses `Heist <https://gitlab.com/saltstack/pop/heist>`_ to deploy and
manage the Salt artifact. Heist-salt only supports managing a single binary,
but support can be added in the future to support other types of binaries.
The single binary is built using `tiamat <https://gitlab.com/saltstack/pop/tiamat>`_.
Tiamat uses `pyinstaller <https://www.pyinstaller.org>`_ and can build either
a onedir or onefile build. The single binary in heist-salt, is a salt artifact built
with pyinstaller's onefile option.


Heist automatically downloads artifacts from `repo.saltproject.io`_ and uses them to deploy
agents. In the case of `Salt`, these artifacts are single binary versions of
`Salt` that have been built using the `salt-pkg`_ system.

Heist will automatically download the latest artifact from the repo, unless
it already exists in the `artifacts_dir` or a specific Salt version is set
via the `artifact_version` option. Heist will automatically detect the target
OS and download the appropriate binary. If `artifact_version` is set, heist
will download the `Salt` binary version from the repo if it exists.

If you want to deploy a custom version of Salt, or you want a salt binary
that includes a different version of python, or more dependency libraries
this is very easy to do using the `salt-pkg`_ project. See the :ref:`custom_artifact`
docs for more information.

When the artifacts are downloaded from the remote repo they are placed in
your `artifacts_dir`. If you are running `heist` as root this dir can be
found at `/var/lib/heist/artifacts`. If you are running as a user this dir
can be found at `~/.heist/artifacts`.

The downloaded executables are in a tarball and are versioned with a version number following
the dash right after the name of the binary. It also includes the OS and architecture.
In the case of `salt` the file looks like this: `salt-3003-3-linux-amd64.tar.gz`. If you have
a custom build just name it something that has a higher version number, like `salt_3003-4-linux-amd64.tar.gz`.


.. _`salt-pkg`: https://gitlab.com/saltstack/open/salt-pkg
.. _`repo.saltproject.io`: https://repo.saltproject.io/salt-singlebin/
