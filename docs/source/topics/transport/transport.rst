Pluggable Tunnel System
=======================

The tunnel system in Heist and Heist Salt is pluggable, but currently
the only supported tunnel is `asyncssh`_. The tunnel plugin system is used
to create connections from heist to the minions. This connection is used
to do the following:

1. Manage files and binaries
----------------------------

The tunnel helps copy binaries and files over to the minion. The `asyncssh`_
tunnel, for example, uses sftp to copy over these files via ssh.

2. Manage a tunnel
------------------

Establish an ssh tunnel from the Heist Salt minions to the Salt Master's publish and
master ports. The `asyncssh`_ tunnel, for example creates an ssh tunnel on the minions
on ports `44505` and `44506` back to the Salt Master's `master_port` and `publish_port`
ports, by default (`4505` and `4506`)

Salt minion's set the `publish_port` to `44505` and the `master_port` to `44506` by
default. You can change the minion's `publish_port` and `master_port` by editing the
minion_opts in your roster.

.. code-block:: yaml

    system_name:
      host: 192.168.1.2
      username: root
      password: "rootpassword"
      minion_opts:
        master_port: 54506
        publish_port: 54505

This will edit the minion config to point the `master_port` to 54506 and the `publish_port`
to 54505. The SSH tunnel will also use these values to setup the tunnel on these ports on
the minion side.


3. Handling Disconnects
-----------------------

The tunnel detects if there is a disconnect and will attemt to re-connect automatically.
The `checkin_time` configuration is used to determine in seconds how often to check if
the connection is established.


.. _`asyncssh`: https://asyncssh.readthedocs.io/en/latest/
