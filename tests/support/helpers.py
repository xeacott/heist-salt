import pathlib


TESTS_DIR = pathlib.Path(__file__).resolve().parent.parent
CODE_DIR = TESTS_DIR.parent
TEST_FILES = TESTS_DIR / "files"
