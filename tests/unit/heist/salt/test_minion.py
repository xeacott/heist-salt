import json
import pathlib
import tarfile
import tempfile
from unittest.mock import call
from unittest.mock import Mock
from unittest.mock import patch

import pytest
from dict_tools.data import NamespaceDict

from tests.support.helpers import TEST_FILES


class TestConfigData:
    def __init__(self):
        self.tname = "12344175aa612d560b0a89917d6b3f7zc3c3c9dcb40b8319a3335e0f0463210a"
        self.remote = {
            "host": "192.168.1.2",
            "username": "root",
            "password": "test_password",
            "id": "testarget_name",
            "tunnel": "asyncssh",
        }
        self.roster = {
            self.tname: {
                "host": "192.168.1.59",
                "username": "root",
                "password": "testpasswd",
                "master_port": 1234,
                "minion_opts": {"master_port": 5678},
                "id": "test_id",
                "tunnel": "asyncssh",
                "bootstrap": False,
            }
        }


@pytest.fixture()
def test_data():
    return TestConfigData()


@pytest.mark.asyncio
async def test_manage_tunnel(hub, mock_hub, test_data):
    """
    test manage_tunnel when creation successfule and no tunnel created
    """
    mock_hub.heist.ROSTERS = test_data.roster
    mock_hub.heist.salt.minion.manage_tunnel = hub.heist.salt.minion.manage_tunnel
    assert await mock_hub.heist.salt.minion.manage_tunnel(
        test_data.tname, "asyncssh", remote=test_data.remote
    )
    assert mock_hub.tunnel.asyncssh.create.call_args_list == [
        call(test_data.tname, test_data.remote, reconnect=False)
    ]
    mock_hub.tunnel.asyncssh.tunnel.assert_not_called()


@pytest.mark.asyncio
async def test_manage_tunnel_with_salt_tunnel(hub, mock_hub, test_data):
    """
    test manage_tunnel when creation successfule and the salt tunnel created
    """
    mock_hub.heist.ROSTERS = test_data.roster
    mock_hub.heist.salt.minion.manage_tunnel = hub.heist.salt.minion.manage_tunnel
    config = mock_hub.tool.config.get_minion_opts(
        run_dir=pathlib.Path("root"), target_name="1234"
    )
    assert await mock_hub.heist.salt.minion.manage_tunnel(
        test_data.tname,
        "asyncssh",
        remote=test_data.remote,
        tunnel=True,
        minion_opts=config,
    )
    assert mock_hub.tunnel.asyncssh.create.call_args_list == [
        call(test_data.tname, test_data.remote, reconnect=False)
    ]
    assert mock_hub.tunnel.asyncssh.tunnel.call_count == 2
