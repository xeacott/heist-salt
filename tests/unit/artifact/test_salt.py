import json
import pathlib
import tarfile
import tempfile
from unittest.mock import call
from unittest.mock import Mock
from unittest.mock import patch

import pytest
from dict_tools.data import NamespaceDict

from tests.support.helpers import TEST_FILES


@pytest.mark.asyncio
async def test_repo_data(hub, mock_hub):
    """
    test repo_data when json returned
    """
    with open(TEST_FILES / "repo.json") as fp:
        repo_data = json.load(fp)
    mock_hub.artifact.salt.repo_data = hub.artifact.salt.repo_data
    mock_hub.artifact.salt.fetch.return_value = repo_data
    mock_hub.OPT = Mock()
    mock_hub.OPT.heist = NamespaceDict(salt_repo_url="https://testrepo.com")
    assert await mock_hub.artifact.salt.repo_data() == repo_data


@pytest.mark.asyncio
async def test_repo_data_none_returned(hub, mock_hub):
    """
    test repo_data when None is returned
    """
    mock_hub.artifact.salt.repo_data = hub.artifact.salt.repo_data
    mock_hub.artifact.salt.fetch.return_value = None
    mock_hub.OPT = Mock()
    mock_hub.OPT.heist = NamespaceDict(salt_repo_url="https://testrepo.com")
    assert not await mock_hub.artifact.salt.repo_data()


@pytest.mark.parametrize(
    "hash_value,expected",
    [
        (
            "3d04197bd5186f1caffa4797dc5c5e0827ff81494b08734cc4871e650a464e2b74e525597a5889fafca613f14e3ff4bc1469e126fba90e064076cf29700e5a3b",
            True,
        ),
        ("1234", False),
    ],
)
def test_verify_hash(hub, mock_hub, hash_value, expected):
    """
    test verify_hash when hash is correct
    """
    mock_hub.artifact.salt.verify_hash = hub.artifact.salt.verify_hash
    if expected:
        assert mock_hub.artifact.salt.verify_hash(
            location=TEST_FILES / "sha_file_test",
            hash_value=hash_value,
            hash_type="sha3_512",
        )
    else:
        assert not mock_hub.artifact.salt.verify_hash(
            location=TEST_FILES / "sha_file_test",
            hash_value=hash_value,
            hash_type="sha3_512",
        )


@pytest.mark.asyncio
async def test_get(hub, mock_hub):
    """
    test salt.get artifact
    """
    mock_hub.artifact.salt.get = hub.artifact.salt.get
    mock_hub.artifact.salt.latest.return_value = False
    mock_hub.artifact.salt.fetch.return_value = True
    mock_hub.artifact.salt.verify_hash.return_value = True
    mock_temp_dir = tempfile.TemporaryDirectory()

    # add artifact
    with tempfile.TemporaryDirectory() as repo_tmpdir:
        with tarfile.open(
            pathlib.Path(mock_temp_dir.name, "salt-3003-3-linux-amd64.tar.gz"), "w:gz"
        ) as tf:
            tf.add(TEST_FILES / "repo.json")

        with open(TEST_FILES / "repo.json") as fp:
            repo_data = json.load(fp)

        artifacts_dir = pathlib.Path(repo_tmpdir) / "repo"
        artifacts_dir.mkdir()

        artifact = artifacts_dir / "salt-3003-3-linux-amd64.tar.gz"
        mock_hub.OPT = Mock()
        mock_hub.OPT.heist = NamespaceDict(
            artifacts_dir=artifacts_dir, salt_repo_url="https://testrepo.com"
        )
        with patch("tempfile.TemporaryDirectory", Mock(return_value=mock_temp_dir)):
            assert (
                await mock_hub.artifact.salt.get(
                    "test_target",
                    "testunnel_plugin",
                    "linux",
                    repo_data=repo_data,
                    version="3003",
                )
                == str(artifact)
            )

        assert artifact.exists()


@pytest.mark.asyncio
async def test_deploy(hub, mock_hub, tmpdir):
    """
    test the correct path is returned when running test_deploy
    """
    mock_hub.artifact.salt.deploy = hub.artifact.salt.deploy
    mock_hub.tunnel.asyncssh.cmd.return_value = NamespaceDict(
        {"returncode": 0, "stderr": 0}
    )
    mock_tempfile = tempfile.TemporaryDirectory()
    run_dir = pathlib.Path("var") / "tmp" / "test_run_dir"
    with tempfile.TemporaryDirectory() as repo_tmpdir:
        binary = pathlib.Path(repo_tmpdir) / "salt-3003-3-linux-amd64.tar.gz"
        with tarfile.open(binary, "w:gz") as tf:
            tf.add(TEST_FILES / "repo.json")

        patch_os = patch("os.remove", Mock(return_value=True))
        patch_tempfile = patch(
            "tempfile.TemporaryDirectory", Mock(return_value=mock_tempfile)
        )
        with patch_os, patch_tempfile:
            ret = await mock_hub.artifact.salt.deploy(
                "test_tunnel_name", "asyncssh", run_dir, binary
            )

        assert mock_hub.tunnel.asyncssh.send.call_args_list[1] == call(
            "test_tunnel_name",
            pathlib.Path(mock_tempfile.name) / "salt",
            run_dir,
            preserve=True,
        )
