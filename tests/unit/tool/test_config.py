import pathlib
from unittest.mock import Mock

import yaml


def test_mk_config_minion_config(hub, mock_hub):
    """
    test mk_config function when passing minion configurations
    """
    mock_hub.tool.config.mk_config = hub.tool.config.mk_config
    mock_hub.tool.config.get_minion_opts = hub.tool.config.get_minion_opts
    tname = "12344175aa612d560b0a89917d6b3f7zc3c3c9dcb40b8319a3335e0f0463210a"
    mock_hub.heist.ROSTERS = {
        tname: {
            "host": "192.168.1.59",
            "username": "root",
            "password": "testpasswd",
            "minion_opts": {"log_level_logfile": "debug"},
            "id": "test_id",
            "tunnel": "asyncssh",
        }
    }

    config = mock_hub.tool.config.get_minion_opts(
        run_dir=pathlib.Path("root"), target_name=tname
    )
    path = mock_hub.tool.config.mk_config(config)

    with open(path) as fp:
        content = yaml.safe_load(fp)

    assert content == {
        "log_level_logfile": "debug",
        "master": "127.0.0.1",
        "master_port": 44506,
        "publish_port": 44505,
        "root_dir": "root/root_dir",
    }


def test_mk_config_minion_config_with_master_port(hub, mock_hub):
    """
    test mk_config function when passing minion configurations
    and setting the same config as master_port
    """
    mock_hub.tool.config.mk_config = hub.tool.config.mk_config
    mock_hub.tool.config.get_minion_opts = hub.tool.config.get_minion_opts
    tname = "12344175aa612d560b0a89917d6b3f7zc3c3c9dcb40b8319a3335e0f0463210a"
    mock_hub.heist.ROSTERS = {
        tname: {
            "host": "192.168.1.59",
            "username": "root",
            "password": "testpasswd",
            "minion_opts": {"master_port": 5678},
            "id": "test_id",
            "tunnel": "asyncssh",
        }
    }

    config = mock_hub.tool.config.get_minion_opts(
        run_dir=pathlib.Path("root"), target_name=tname
    )
    path = mock_hub.tool.config.mk_config(config)

    with open(path) as fp:
        content = yaml.safe_load(fp)

    assert content == {
        "master": "127.0.0.1",
        "master_port": 5678,
        "publish_port": 44505,
        "root_dir": "root/root_dir",
    }
