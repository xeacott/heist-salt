import pytest


@pytest.fixture(scope="function")
def hub(hub):
    for dyne in ("config", "heist", "salt", "artifact", "tool", "tunnel"):
        hub.pop.sub.add(dyne_name=dyne)
    yield hub
